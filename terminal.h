#ifndef TERMINAL_H
#define TERMINAL_H

#include <QObject>
#include <QTextEdit>
#include <QGridLayout>

#include <QDebug>


class terminal   : public QWidget
{
    Q_OBJECT
public:
    terminal();
    ~terminal();


private:
    QGridLayout*            mainLayout;
    QTextEdit*              console;
};

#endif // TERMINAL_H
