#-------------------------------------------------
#
# Project created by QtCreator 2015-10-01T21:03:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = luciminal
TEMPLATE = app


SOURCES += main.cpp
SOURCES += terminal.cpp
SOURCES += mainwindow.cpp

HEADERS  += terminal.h
HEADERS  += mainwindow.h


FORMS    += mainwindow.ui



# Add this lines to use the rob-O-matic librairy
INCLUDEPATH     += usr/include
LIBS            += -lrobomatic
LIBS            += -lz

# Create temporary directories
OBJECTS_DIR     = tmp
MOC_DIR         = tmp
DESTDIR         = bin

